package uz.pdp.appregiondistrictjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppRegionDistrictJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppRegionDistrictJpaApplication.class, args);
    }

}
