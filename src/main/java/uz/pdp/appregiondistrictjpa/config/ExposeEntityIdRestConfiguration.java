package uz.pdp.appregiondistrictjpa.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import uz.pdp.appregiondistrictjpa.entity.District;
import uz.pdp.appregiondistrictjpa.entity.Region;
import uz.pdp.appregiondistrictjpa.repository.projection.RegionProjection;

@Configuration
public class ExposeEntityIdRestConfiguration implements RepositoryRestConfigurer {


    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
        config.exposeIdsFor(Region.class);
        config.exposeIdsFor(District.class);
    }
}