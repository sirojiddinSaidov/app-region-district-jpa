package uz.pdp.appregiondistrictjpa.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name_uz","region_id"})
})
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false,name = "name_uz")
    private String nameUz;

    @ManyToOne(optional = false)
    @JoinColumn(name = "region_id")
    private Region region;
}