package uz.pdp.appregiondistrictjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appregiondistrictjpa.entity.District;

@RepositoryRestResource(path = "district")
public interface DistrictRepository extends JpaRepository<District, Integer> {
}