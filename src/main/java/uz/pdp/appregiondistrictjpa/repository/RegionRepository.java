package uz.pdp.appregiondistrictjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appregiondistrictjpa.entity.Region;
import uz.pdp.appregiondistrictjpa.repository.projection.RegionProjection;

@RepositoryRestResource(path = "region", excerptProjection = RegionProjection.class)
public interface RegionRepository extends JpaRepository<Region, Integer> {
}