package uz.pdp.appregiondistrictjpa.repository.projection;

import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.appregiondistrictjpa.entity.District;
import uz.pdp.appregiondistrictjpa.entity.Region;

import java.util.List;

@Projection(types = Region.class)
public interface RegionProjection {

    Integer getId();

    String getName();

    List<District> getDistricts();
}
